package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    var newOp = true
    var oldNum = ""
    var op = "+"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun clickEvent(view: android.view.View) {
        if (newOp)
            editText.setText("")
        newOp = false
        var btnClick = editText.text.toString()
        var btnSelect = view as Button
        when(btnSelect.id){
            nbutton1.id -> {btnClick += "1"}
            nbutton2.id -> {btnClick += "2"}
            nbutton3.id -> {btnClick += "3"}
            nbutton4.id -> {btnClick += "4"}
            nbutton5.id -> {btnClick += "5"}
            nbutton6.id -> {btnClick += "6"}
            nbutton7.id -> {btnClick += "7"}
            nbutton8.id -> {btnClick += "8"}
            nbutton9.id -> {btnClick += "9"}
            nbutton0.id -> {btnClick += "0"}
            dot.id -> {btnClick += "."}
            plusMinus.id -> {btnClick = "-$btnClick"}




            
        }
        editText.setText(btnClick)
    }

    fun operationEvent(view: android.view.View) {
        newOp = true
        oldNum = editText.text.toString()
        var btnSelect = view as Button
        when(btnSelect.id){
            multiply.id -> {op = "*"}
            divide.id -> {op = "/"}
            add.id -> {op = "+"}
            subtract.id -> {op = "-"}
        }

    }

    fun equalEvent(view: android.view.View) {
        var newNum = editText.text.toString()
        var result = 0.0
        when(op){
            "+" -> {result = oldNum.toDouble() + newNum.toDouble()}
            "*" -> {result = oldNum.toDouble() * newNum.toDouble()}
            "/" -> {result = oldNum.toDouble() / newNum.toDouble()}
            "-" -> {result = oldNum.toDouble() - newNum.toDouble()}
        }
        editText.setText(result.toString())
    }

    fun clearEvent(view: android.view.View) {
        editText.setText("0")
        newOp = true

    }

    fun percentEvent(view: android.view.View) {
        var number = editText.text.toString().toDouble()/100
        editText.setText(number.toString())
        newOp = true


    }
}